/**
 * init.js
 *
 * This is the initial src file used by Browserify
 */

var $ = require('jquery');

// 
// Set up fast click
//
require('fastclick')(document.body);

//
// Icon Loading
// https://css-tricks.com/ajaxing-svg-sprite/
//
$.get("/assets/icons/icons.svg", function(data) {
  var div = document.createElement("div");
  div.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
  document.body.insertBefore(div, document.body.childNodes[0]);
});

// Sample Revealing Module, delete or modify for your projects
var SampleRevealingModule = require('app/modules/SampleRevealingModule');
SampleRevealingModule.init();
