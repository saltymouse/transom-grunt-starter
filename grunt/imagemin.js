module.exports = {
  all: {
    files: [{
      expand: true,
      cwd: '<%= globalConfig.src %>/',
      src: ['<%= globalConfig.dest_images %>/**/*.{png,jpg,gif,svg,cur,ico}'],
      dest: '<%= globalConfig.dest %>'
    }]
  }
};