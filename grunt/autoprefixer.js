module.exports = {
  // Development settings
  dist: {
    options: {
      map: true
    },
    expand: true,
    cwd: '<%= globalConfig.dest %>/<%= globalConfig.dest_stylesheets %>/', // Same as sass:
    src: '{,*/}*.css',
    dest: '<%= globalConfig.dest %>/<%= globalConfig.dest_stylesheets %>/' // Same as sass:
  }
};