module.exports = {
  fonts: {
    files: [{
      expand: true,
      cwd: '<%= globalConfig.src %>/fonts/',
      src: ['**'],
      dest: '<%= globalConfig.dest %>/fonts/'
    }],
  }
};